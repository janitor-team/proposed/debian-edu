#!/usr/bin/make -f

TASKSELOPTS = -u
GENCONTROL_DEPENDS = true

include /usr/share/blends-dev/Makefile

# override target from /usr/share/blends-dev/Makefile
# this is a workaround for two missing features in blends-dev (we should probably file wishlist bugs)
# a.) depends on arch:any packages should refer binary version to allow binNMUs (see #910498)
# b.) remove empty trailing line in debian/control to avoid pedantic lintian warning
dist:
	        rm -f $(BLEND)-tasks.desc debian/control
	        make -f debian/rules get-orig-source
		sed -i 's#source:Version#binary:Version#' debian/control
		perl -i -pe "chomp if eof" debian/control
